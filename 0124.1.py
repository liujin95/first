import torch

# batch_n表示一个批次输入数据的数量；同时。每个数据包含有数据特征input_data个
# hidden_layer用于定义经过隐藏层后保留的数据特征个数，本网络只有一层隐藏层。output_data是输出的数据
batch_n=100
hidden_layer=100
input_data=1000
output_data=10

#初始化权重
x=torch.randn(batch_n,input_data)
y=torch.randn(batch_n,output_data)

w1=torch.randn(input_data,hidden_layer)
w2=torch.randn(hidden_layer,output_data)

#明确训练次数和学习效率
epoch_n=20
lenrning_rate=1e-6

#梯度下降优化神经网络
for epoch in range(epoch_n):
    h1=x.mm(w1)  #100*1000
    h1=h1.clamp(min=0)
    y_pred=h1.mm(w2)  #100*10

    loss=(y_pred-y).pow(2).sum()
    print("Epoch:{},loss:{:.4f}".format(epoch,loss))

    gray_y_pred=2*(y_pred-y)
    gray_w2=h1.t().mm(gray_y_pred)

    gray_h=gray_y_pred.clone()
    gray_h=gray_h.mm(w2.t())
    gray_h.clamp_(min=0)
    gray_w1=x.t().mm(gray_h)

    w1-=lenrning_rate*gray_w1
    w2-=lenrning_rate*gray_w2
